# TextLayoutBuilder

## Introduction

This library is adapted based on the source library [TextLayoutBuilder](https://github.com/facebook/TextLayoutBuilder) to run on OpenHarmony. TextLayoutBuilder is a tool for building text layouts, including the font spacing, size, color, layout mode, and rich text highlighting. It is used in all text display scenarios, especially when the font style is specially displayed for notifications, greatly improving the development efficiency.

## Effect
![gif](preview_EN.gif)

## How to Install

 ```
 ohpm install @ohos/textlayoutbuilder 
 ```
For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).


## How to Use

Step 1: Import the TextLayout component to your project and instantiate the TextLayout.Layout object.
```typescript
...
import TextLayout from '@ohos/textlayoutbuilder';
import {TextInfo} from '@ohos/textlayoutbuilder';
...
@State layout:TextLayout.Layout = new TextLayout.Layout(); 
...

```
Step 2:: Set UI attributes through the **layout** class object to define the required style. You can also add required callbacks.
```typescript
private aboutToAppear() {

    let textInfo = new TextInfo();
    textInfo.setStart(2)
    textInfo.setEnd(8)
    textInfo.setFontColor('#ff0000')
    ... ...

    this.layout
    .setText(this.strTest)
    .setSpecialTextInfo(textInfo1)
    .setSpecialTextInfo(textInfo2)
    .setSpecialTextInfo(textInfo3)
    .setSpecialTextClick((textInfo) =>{
    console.info ('I clicked = '+textInfo.getText())
    });
}
```
Step 3: Draw a page and transfer the customized layout to TextLayout.
```typescript
build() {
    Column() {
         Text ("Displayed by default").fontSize(16).fontColor("#999999").margin({ left: 14, top: 14, bottom: 14 })
         Column() {
            TextLayout({ model: this.layout })
         }.backgroundColor("#cccccc").margin({ left: 14, right: 14 }).borderRadius(10)
    }.alignItems(HorizontalAlign.Start)
}
```
## Available APIs
```
let layout:TextLayout.Layout = new TextLayout.Layout();
```
1. Set the text content using layout.setText(text: string).

   **Parameters**

   | Parameter      | Type   | Mandatory |Description|
   | :---------- | ----------| -----|-----  |
   |  text       | string    | Yes  | Text content   |

2. Set the specified text style using layout.setSpecialTextInfo(specialTextInfo: TextInfo).

   **Parameters**

   | Parameter      | Type   | Mandatory |Description|
   | :---------- | ----------| -----|-----  |
   |  specialTextInfo  | TextInfo    | Yes  | Text content style|

3. Set the text color using layout.setTextColor(textColor: string.|Color|number)

   **Parameters**

   | Parameter      | Type   | Mandatory |Description|
   | :---------- | ----------| -----|-----  |
   |  textColor    | string&#124;Color&#124;number | Yes  | Text color   |

4. Set whether to display in a single line using layout.setSingleLine(singleLine: boolean).

   **Parameters**

   | Parameter      | Type   | Mandatory |Description|
   | :---------- | ----------| -----|-----  |
   | singleLine    | boolean    | Yes  | Whether to display in a single line. The default value is No.   |

5. Set the maximum number of lines using layout.setMaxLines(maxLines: number).

   **Parameters**

   | Parameter      | Type   | Mandatory |Description|
   | :---------- | ----------| -----|-----  |
   |   maxLines   | number   | Yes  | Maximum number of lines to set.   |

6. Set the ellipsis replacement mode using layout.setEllipsize(ellipsize: TextOverflow) when the text cannot be completely displayed.

   **Parameters**

   | Parameter      | Type   | Mandatory |Description|
   | :---------- | ----------| -----|-----  |
   |ellipsize   | TextOverflow | Yes  | Text overflow display mode.   |
  
7. Set the text alignment mode using layout.setAlignment(textalign: TextAlign).

   **Parameters**

   | Parameter      | Type   | Mandatory |Description|
   | :---------- | ----------| -----|-----  |
   | textalign  | TextAlign | Yes  | Text alignment mode.   |

8. Set the minimum number of characters displayed in each line using layout.setMinEms(minEms: number).

   **Parameters**

   | Parameter      | Type   | Mandatory |Description|
   | :---------- | ----------| -----|-----  |
   |minEms     | number | Yes | Number of characters displayed in each line. |

9. Set whether to set the inner margin of the component using layout.setIncludeFontPadding(shouldInclude: boolean).

   **Parameters**

   | Parameter      | Type   | Mandatory |Description|
   | :---------- | ----------| -----|-----  |
   | shouldInclude | boolean   | Yes  | Whether to set padding. The default value is No.|

10. Setting the maximum width using layout.setMaxWidth(maxWidth: number).

    **Parameters**

    | Parameter      | Type   | Mandatory |Description|
    | :---------- | ----------| -----|-----  |
    | maxWidth | number  | Yes | Maximum width|


11. Set whether to enable the status change function using layout.setIsEnablePressState(isEnablePressState: boolean) when the text is touched.

    **Parameters**

    | Parameter      | Type   | Mandatory |Description|
    | :---------- | ----------| -----|-----  |
    | isEnablePressState  | boolean | Yes| Whether to enable the status change function when the text is pressed. By default, the function is disabled.|

12. Set the status style of the pressed text using layout.setTextPressStateStyle(textPressStateStyle: string).

    **Parameters**

    | Parameter      | Type   | Mandatory |Description|
    | :---------- | ----------| -----|-----  |
    |textPressStateStyle | string | Yes  | Style of the pressed text status   |

13. Set the click event of the specified text using layout.setSpecialTextClick(clickCallback: (textInfo: TextInfo) => void).

    **Parameters**

    | Parameter      | Type   | Mandatory |Description|
    | :---------- | ----------| -----|-----  |
    | clickCallback | (textInfo: TextInfo) => void   | Yes | Custom callback.   |

```
Set special character attributes using let textInfo = new TextInfo().
```
1. Set the start position of the special text using textInfo.setStart(start:number).

   **Parameters**

   | Parameter      | Type   | Mandatory |Description|
   | :---------- | ----------| -----|-----  |
   | start  | number | Yes | Start position of the special text. |

2. Set the end position of the special text using textInfo.setEnd(end:number).

   **Parameters**

   | Parameter      | Type   | Mandatory |Description|
   | :---------- | ----------| -----|-----  |
   | end | number  |Yes | End position of the special text. |

3. Set the text color using textInfo.setFontColor(fontColor:string).

   **Parameters**

   | Parameter      | Type   | Mandatory |Description|
   | :---------- | ----------| -----|-----  |
   | fontColor   | string    | Yes| Font color.  |

4. Set the text content using textInfo.setText(text:string).

   **Parameters**

   | Parameter      | Type   | Mandatory |Description|
   | :---------- | ----------| -----|-----  |
   | text      | string  |Yes  | Text content.   |

5. Set the text type using textInfo.setTextType(textType:string).

   **Parameters**

   | Parameter      | Type   | Mandatory |Description|
   | :---------- | ----------| -----|-----  |
   | textType     | string | Yes  |Text type (TEXT_TYPE_NORMAL&#124;TEXT_TYPE_RICH&#124;TEXT_TYPE_HTTP).  |

6. Set the distance between text fonts using textInfo.setFontLetterSpacing(fontLetterSpacing:string).

   **Parameters**

   | Parameter      | Type   | Mandatory |Description|
   | :---------- | ----------| -----|-----  |
   | fontLetterSpacing  | string | Yes  |Distance between text fonts. |

7. Set the font size using textInfo.setFontSize(fontSize:string).

   **Parameters**

   | Parameter      | Type   | Mandatory |Description|
   | :---------- | ----------| -----|-----  |
   | fontSize    | string  | Yes | Font size.|

8. Set the font style using textInfo.setFontStyle(fontStyle:string).

   **Parameters**

   | Parameter      | Type   | Mandatory |Description|
   | :---------- | ----------| -----|-----  |
   | fontStyle  | string  | Yes | Font style.|

## Constraints

This project has been verified in the following version:

- DevEco Studio: NEXT Release(5.0.3.900), SDK: API12 (5.0.0.71)

- DevEco Studio: 3.1 Beta2 (3.1.0.400), SDK: API 9 Release (3.2.11.9)

## Directory Structure

```
|---- TextLayoutBuilder
|     |---- entry  # Sample code
|     |---- library  # TextLayout library
|          |---- src
|            |---- main
|              |---- ets
|                  |---- commonents
|                       |---- TextInfo.ets  # Rich text data entity
|                       |---- TextLayout.ets  # UI custom component
|           |---- index.ets  # External APIs
|     |---- README.md  # Readme
```

## How to Contribute
If you find any problem during the use, submit an [Issue](https://gitee.com/openharmony-sig/TextLayoutBuilder/issues) or a [PR](https://gitee.com/openharmony-sig/TextLayoutBuilder/pulls) to us.

## License
This project is incensed under [Apache License 2.0](https://gitee.com/openharmony-sig/TextLayoutBuilder/blob/master/LICENSE).
